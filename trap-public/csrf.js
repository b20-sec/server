let i=1;
function f(e){
    e.preventDefault();
    let w = open('about:blank', 'a');
    if(w){
        if(open('/question.html', `${new Date().getTime()}`, `x=0,y=0,width=${window.parent.screen.width},height=${window.parent.screen.width}`)){
            document.getElementById('click').removeEventListener('click', f);
            this.click();
            w && setTimeout(()=>w.close(), 100);
            document.getElementById('click').addEventListener('click', f, {passive:false});
        }else{
            w.close();
            alert('アンケートに回答するため、ポップアップを許可してください');
        }
    }else{
        alert('アンケートに回答するため、ポップアップを許可してください');
    }
}
document.getElementById('click').addEventListener('click', f, {passive:false});