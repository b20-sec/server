'use strict';
(function(){
    let openOrig = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function(...args){
        let url = args[1];
        if(typeof url !== 'string' || url.constructor !== ('').constructor || x !== String(x)){
            
        }
        if(url.match(/^(https?:)?\/\//) && !url.match(/^(http:)?\/\/localhost:3000\//)){
            throw new Error(`Not Allowed Origin : ${url.replace(/^(https?:)?\/\//,'').split('/')[0]}`);
        }
        return openOrig.call(this, ...args);
    };
})();