document.addEventListener('DOMContentLoaded',function(){
var input = document.getElementById('pw-input'),
    button = document.getElementById('pw-show-button');
$('#pw-show-button').mousedown(function(evt){
    if(!navigator.userAgent.toLocaleLowerCase().match(/mobile/))
        show();
})
document.onmouseup = hide;
document.onmouseleave = hide;
document.getElementById('pw-show-button').addEventListener('touchstart', show);
document.addEventListener('touchend', hide);
function show(){
    input.type = 'text';
    button.style.opacity = '0';
}
function hide(){
    input.type = 'password';
    button.style.opacity = '1';
}
});