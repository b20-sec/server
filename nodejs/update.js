function update(client){
return new Promise((resolve, reject)=>{
    //ここにfor文とか書いてね
    //全部が終わったらresolve()してね
    let fin_count = 0; //終わったクエリを数える。全部終わったら　resolve
    for(let i=0; i<5; i++){
        client.query('INSERT INTO bbs(author, text) VALUES($1, $2)', [`名無し${i}`, `てすとNo.${i}です`])
        .then(rslt =>{
            fin_count++;
            console.log(`Finished ${fin_count}`);
            if(fin_count == 5){
                resolve();
            }
        });
    }
});
}
const
    {Pool} = require('pg'),
    pool = new Pool({host:'localhost', user:process.env.USERNAME, database:process.env.USERNAME, password:'psql'});
pool.connect()
    .then(client => {
        try{
            require('child_process').exec('psql -f ../update.sql', (err, stdout, stderr) => {
                if (err)
                    console.error(err);
                console.log(stdout);
                if(stderr.length)
                    console.error(stderr);
                console.log('Begin update()...');
                update(client).then(()=>{
                    client.release();
                    process.exit(0);
                });
            });
        }catch(e){
            console.error(e);
        }
    },err => {
        if(err.errno === 'ECONNREFUSED'){
            console.error('Cannot connect to PostgreSQL Server.\nIs it active?\n');
        }else
            throw err;
    }
).catch(err => {throw err;});