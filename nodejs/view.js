const
    fs  = require('fs'),
    ejs = require('ejs');

module.exports = read(fs.readdirSync('view'), 'view/');
function read(list, startDir){
    let rtn = {};
    for(let entry of list){
        if(fs.statSync(`${startDir}${entry}`).isDirectory())
            rtn[entry] = read(fs.readdirSync(`${startDir}${entry}`),`${startDir}${entry}/`);
        else if(entry.match(/.ejs$/) !== null){
            let file = fs.readFileSync(`${startDir}${entry}`, 'utf-8');
            rtn[entry.replace(/.ejs$/,'')] = (param, opts=undefined) => ejs.render(file, param, opts);
        }
    }
    return rtn;
}