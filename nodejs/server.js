'use strict';
const
    PORT = 3000,
    {Pool}   = require('pg'),
    http     = require('http'),
    https    = require('https'),
    fs       = require('fs'),
    qs       = require('qs'),
    express  = require('express'),
    bcrypt   = require('bcrypt'),
    bcryptRounds = 12,
    crypto   = require('crypto'),
    view     = require('./view'),
    pool     = new Pool({host:'localhost', user:process.env.USERNAME, database:process.env.USERNAME, password:'psql'}),
    app      = express(),
    server = http.createServer(app),
    statusCode = {
        'OK': 200,
        'SeeOther': 303,
        'BadRequest': 400,
        'NotFound': 404,
        'Forbidden': 403,
        'InternalServerError': 500
    };
express.static.mime.define({'text/html; charset=UTF8': ['html','htm']});
express.static.mime.define({'text/css; charset=UTF8': ['css']});
express.static.mime.define({'text/javascript; charset=UTF8': ['js']});
express.static.mime.define({'image/png': ['png']});
express.static.mime.define({'image/jpeg': ['jpeg', 'jpg']});
pool.on('error', (err, client)=>{
    console.error('Unexpected error on PostgreSQL client', err);
    process.exit(-1);
});
pool.connect()
    .then(client => {
        try{
            client.connectionParameters.password = undefined;
            client.password = undefined;
            console.log('Connection to PostgreSQL is established succesfully!');
            start(client);
        }catch(e){
            console.error(e);
        }
    },err => {
        if(err.errno === 'ECONNREFUSED' || err.errno === -111){
            console.error('Cannot connect to PostgreSQL Server.\nIs it active?\n');
        }else{
            throw err;
        }
    }
).catch(err => {throw err;});
function start(client){
// HTTPserver.on('request',(req, res)=>{
//     response(res, statusCode.SeeOther,'',{Location:`https://server.url${req.url}`});
// });
app.use(express.static('../public'));
app.get('/',(req, res)=>{
    response(res, statusCode.OK, view.index({}), {}, {type:'html'});
});

app.get('/top',(req,res)=>{
    response(res,statusCode.OK,view.top({}),{},{type:'html'});
});

//クソ実装の始まり
app.get('/first_injection',(req,res)=>{
    response(res,statusCode.OK,view.first_injection({id_err:true,passwd_err:true}),{},{type:'html'});
});
app.post('/first_injection',(req, res)=>{
    let bodyArr = [];
    req.on('data',chunk=>bodyArr.push(chunk));
    req.on('end',()=>{
        let body = Buffer.concat(bodyArr).toString(),
            data = qs.parse(body),
            res_id = false, 　　　//こいつと
            res_passwd = false;　//こいつずっとfalseだけど要る？
        client.query(`SELECT uid from users WHERE name=\'${data.id}\' AND pw=\'${data.passwd}\'`)
            .then(result => {
                if(result.rows[0] != undefined){
                    response(res,statusCode.OK, view.success_1({}), {}, {type:'html'});
                }
                else{
                    res_id = false;
                    res_passwd = false; //↓200じゃなくて400とかが良さそうな気もする
                    response(res,statusCode.OK, view.first_injection({id_err:res_id,passwd_err:res_passwd}), {}, {type:'html'});
                }
            },err=>{
                console.error(err.toString());
                res_id = false;
                res_passwd = false;
                //ここの500の返し方分からんかった  ←適当な文字列またはBuffer入れればおk
                response(res,statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});　//おk (ほんとはhtml返すべき)(エラーページ作るかなぁ)
            }
        );
    });
});


app.get('/second_injection',(req,res)=>{
    let value = req.query.value;
    if(value == undefined){
        value = 1e9;
    }
    client.query(`SELECT value,name from product WHERE value < ${value}`)
        .then(result => {
            if(result.rows[0] != undefined){
                response(res,statusCode.OK, view.second_injection({data:result.rows,val:value}), {}, {type:'html'});
            }
            else{
                response(res,statusCode.OK, view.second_injection({data:result.rows,val:value}), {}, {type:'html'});
            }
        },err=>{
            console.error(err.toString());
            response(res,statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});
        }
    );
});
app.get('/second_injection_taisaku1',(req,res)=>{
    let value = req.query.value;
    if(value == undefined){
        value = 1e9;
    }
    client.query('SELECT value,name from product WHERE value < $1', [value])
        .then(result => {
            if(result.rows[0] != undefined){
                response(res,statusCode.OK, view.second_injection({data:result.rows,val:value}), {}, {type:'html'});
            }
            else{
                response(res,statusCode.OK, view.second_injection({data:result.rows,val:value}), {}, {type:'html'});
            }
        },err=>{
            console.error(err.toString());
            response(res,statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});
        }
    );
});
app.get('/second_injection_taisaku2',(req,res)=>{
    let value = parseInt(req.query.value);
    if(isNaN(value)){
        value = 1e9;
    }
    client.query('SELECT value,name from product WHERE value < $1', [value])
        .then(result => {
            if(result.rows[0] != undefined){
                response(res,statusCode.OK, view.second_injection({data:result.rows,val:value}), {}, {type:'html'});
            }
            else{
                response(res,statusCode.OK, view.second_injection({data:result.rows,val:value}), {}, {type:'html'});
            }
        },err=>{
            console.error(err.toString());
            response(res,statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});
        }
    );
});
//ここまでクソ実装をお送りしました.

app.get('/requireLogin[1-9]', (req, res) => {
    requireLogin(res, name => {
        response(res, statusCode.OK, view.requireLogin({name:name}),{},{type:'html'});
    });
});

app.get('/user/*', (req, res)=>{
    let message = undefined,
        name = decodeURIComponent(req.url.replace(/^\/user\//,'').split('?')[0]),
        SetCookie = [];
    switch(parseCookie(req.headers.cookie).done){
        case undefined:
            break;
        case 'registerUser':
            message = 'アカウントを作成しました';
            SetCookie.push('done=; Path=/user; Max-Age=0');
            break;
    }
    
    client.query('SELECT * from users WHERE name=$1', [name])
    .then(rslt1 =>{
        let user = rslt1.rows[0];
        if(user != undefined){
            response(res,statusCode.OK,view.user({name:user.name, message:message}),{'Set-Cookie':SetCookie},{type:'html'});
        }else
        response(res,statusCode.OK,view.user({name:undefined.name, message:undefined}),{'Set-Cookie':SetCookie},{type:'html'});
    }, err =>{
        response(res,statusCode.InternalServerError,view.user({err:err, name:user.name, message:message}),{'Set-Cookie':SetCookie},{type:'html'});
        throw err;
    });
});

app.get('/logout',(req, res)=>{
    response(res, statusCode.OK, view.logout({}), {
        'Set-Cookie': [
            'name=; Path=/; Max-Age=0;',
            'key=; Path=/; Max-Age=0;',
            'loginFlag=; Path=/; Max-Age=0;'
        ]
    },{type:'html'});
});
app.get('/login[1-9]',(req, res)=>{
    let cookie = parseCookie(req.headers.cookie), url=undefined;
    if(cookie.url && cookie.url.length>0){
        url = cookie.url;
    }
    response(res, statusCode.OK, view.login({name:undefined, err:undefined, url:url}),{'Set-Cookie':[
        'url=; Path=/login; Max-Age=0;'
    ]},{type:'html'});
});
app.post('/login[1-9]',(req, res)=>{
    let bodyArr = [];
    req.on('data',chunk=>bodyArr.push(chunk));
    req.on('end',()=>{
        let body = Buffer.concat(bodyArr).toString(),
            data = qs.parse(body);
        login(data.name, data.pw, req.url)
        .then(key =>{
            if(req.url == '/login1'){
                response(res, statusCode.SeeOther, '', {Location: data.url||'/',
                    'Set-Cookie' : [
                        `url=; Path=${req.url}; Max-Age=0;`,
                        `name=${data.name}; Path=/; Max-Age=86400;`,// HttpOnly;`,// Secure`,
                        `loginFlag=1; Max-Age=86400;`
                    ]
                });
            }else{
                response(res, statusCode.SeeOther, '', {Location: data.url||'/',
                    'Set-Cookie' : [
                        `url=; Path=${req.url}; Max-Age=0;`,
                        `name=${data.name}; Path=/; Max-Age=86400;`, // HttpOnly;`,// Secure`,
                        `key=${key}; Path=/; Max-Age=86400;` // HttpOnly;`,// Secure`
                    ]
                });
            }
        }, err =>{
            if(err === 'Invalid UserName or PW'){
                response(res, statusCode.BadRequest, view.login({url:data.url, name:data.name,err:'ユーザー名またはパスワードが間違っています'}), {}, {type:'html'}); 
            }else if(err === 'Invalid Character \"\'\"'){
                response(res, statusCode.BadRequest, view.login({url:data.url, name:data.name,err:'無効な文字が使用されています。'}), {}, {type:'html'}); 
            }else{
                response(res, statusCode.InternalServerError, err);
                throw err;
            }
        });
    });
});
app.get('/registerUser',(req, res)=>{
    response(res, statusCode.OK, view.registerUser({name:undefined, err:undefined, errField:{}}),{},{type:'html'});
}); 
app.post('/registerUser', (req, res)=>{
    let bodyArr = [];
    req.on('data',chunk=>bodyArr.push(chunk));
    req.on('end',()=>{
        let body = Buffer.concat(bodyArr).toString(),
            data = qs.parse(body),
            errText='', errField = {};
        if(!data.name||data.name===''){
            errText += (errText===''?'':'<br>')+ 'ユーザー名は必須項目です';
            errField.name = true;
        } else if(data.name.match(/[^\w]/)){
            errText += (errText===''?'':'<br>')+ 'ユーザー名に使える文字は半角英数字と_(アンダーバー)です';
            errField.name = true;
        }
        if(!data.pw||data.pw==''){
            errText += (errText===''?'':'<br>')+ 'パスワードは必須項目です';
            errField.pw = true;
        }else if(data.pw.match(/[^\w#\-\+\(\)\{\}\[\]\'\"\*=\|\/\\\?\.,\^\$%@!`~]/) !== null){
            errText += (errText===''?'':'<br>')+ 'パスワードに使用できない文字が含まれています';
            errField.pw = true;
        }else if(data.pw2!==data.pw){
            errText += (errText===''?'':'<br>')+ 'パスワードの確認が一致しません';
            errField.pw2 = true;
        }
        if(errText != ''){
            response(res,statusCode.BadRequest, view.registerUser({name:data.name, err:errText, errField:errField}), {}, {type:'html'}); 
            return;
        }
        registerUser(data.name, data.pw).then(rslt =>{
            login(data.name, data.pw, '/registerUser')
            .then(key =>{
                response(res, statusCode.SeeOther, '', {Location: `/user/${data.name}`,
                    'Set-Cookie' : [
                        'url=; Path=/login; Max-Age=0;',
                        `done=registerUser; Path=/user/${data.name};`,// Secure`,
                        `name=${data.name}; Path=/; Max-Age=86400;`,// HttpOnly;`,// Secure`,
                        `key=${key}; Path=/; Max-Age=86400;`,// HttpOnly;`,// Secure`
                    ]
                });
            },err =>{
                if(err === 'Invalid UserName or PW')
                    response(res, statusCode.BadRequest, view.login({url:undefined, name:data.name, err:'ユーザー名またはパスワードが間違っています'}), {}, {type:'html'}); 
                else{
                    response(res, statusCode.InternalServerError, err);
                    throw err;
                }
            });
        },err => {
            if(err.routine === '_bt_check_unique'&& err.constraint === 'users_name_key')
                response(res,statusCode.BadRequest, view.registerUser({name:data.name, err:`そのユーザー名(${data.name})は既に登録されています`, errField:{name:true}}), {}, {type:'html'}); 
            else{
                response(res, statusCode.InternalServerError, err);
                throw err;
            }
        });
    });
});
app.get('/bbs[1-2]', (req, res) => {
    client.query('SELECT id, t, author, text FROM bbs').then(rslt => {
        response(res, statusCode.OK, view[req.url.slice(1, 5)]({posts:rslt.rows, err:{}, author:'', text:''}), {}, {type:'html'});
    },
    err => {
        console.error(err.toString());
        response(res, statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});
    });
});
app.post('/bbs[1-2]', (req, res)=>{
    let bodyArr = [];
    req.on('data',chunk=>bodyArr.push(chunk));
    req.on('end',()=>{
        let body = Buffer.concat(bodyArr).toString(),
            data = qs.parse(body), err;
            data.author = data.author ? data.author.replace(/[\r\n]/g,'') : '';
            data.text = data.text ? data.text.replace(/\r\n/g,'\n') : '';
            if(data.author == '' || 50 < data.author.length){
                err = {
                    text : '投稿者名は50字までです。',
                    field : 'author'
                }
            }
            else if(data.text == '' || 280 < data.text.length){
                err = {
                    text : '本文は140字までです。',
                    field : 'text'
                }
            }
            if(err){
                client.query('SELECT id, t, author, text FROM bbs').then(rslt => {
                    response(res, statusCode.OK, view[req.url.slice(1, 5)]({posts:rslt.rows, err:err, author:data.author, text:data.text}), {}, {type:'html'});
                },
                err => {
                    console.error(err.toString());
                    response(res, statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});
                });
            }else{
                client.query('INSERT INTO bbs(author, text) VALUES($1, $2)', [data.author, data.text]).then(() =>{
                    response(res, statusCode.SeeOther, '', {Location : req.url});
                },
                err => {
                    console.error(err.toString());
                    response(res, statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});
                });
            }
    });
});
app.get('/changePW', (req, res)=>{
    requireLogin(res, ()=>{
        response(res, statusCode.OK, view.changePW({errText:'', errField:{}}), {}, {type:'html'});
    });
});
app.post('/changePW', (req, res)=>{
    let bodyArr = [];
    requireLogin(res, (name, uid, key) => {
        req.on('data',chunk=>bodyArr.push(chunk));
        req.on('end',()=>{
            let body = Buffer.concat(bodyArr).toString(),
                data = qs.parse(body),
                errText='', errField = {};
            if(!data.pw||data.pw==''){
                errText += (errText===''?'':'<br>')+ 'パスワードは必須項目です';
                errField.pw = true;
            }else if(data.pw.match(/[^\w#\-\+\(\)\{\}\[\]\'\"\*=\|\/\\\?\.,\^\$%@!`~]/) !== null){
                errText += (errText===''?'':'<br>')+ 'パスワードに使用できない文字が含まれています';
                errField.pw = true;
            }else if(data.pw2 !== data.pw){
                errText += (errText===''?'':'<br>')+ 'パスワードの確認が一致しません';
                errField.pw2 = true;
            }
            if(errText != ''){
                response(res,statusCode.BadRequest, view.changePW({errText:errText, errField:errField}), {}, {type:'html'}); 
                return;
            }
            bcrypt.hash(data.pw, bcryptRounds)
            .then(hash => {
                return client.query('UPDATE USERS SET pw=$1, hash=$2 WHERE name = $3',[data.pw, hash, name]);
            },err => {
                console.error(err);
                response(res, statusCode.InternalServerError, '500エラーだよ', {}, {type:'html'});
            }).then(()=>{//begin debug
                return client.query('select * from login where uid=$1 AND key=$2',[uid, key]);
            },
            err =>{
                console.error(err);
                response(res, statusCode.InternalServerError, '500エラーだよ',{}, {type:'html'});
            }).then(rslt=>{
                console.log(rslt.rows); //end debug
                return client.query('DELETE from login where uid=$1 AND key=$2',[uid, key]);
            },
            err =>{
                console.error(err);
                response(res, statusCode.InternalServerError, '500エラーだよ',{}, {type:'html'});
            }).then(()=>{
                response(res, statusCode.OK, view.changePW_done({}), {'Set-Cookie': [
                    'name=; Path=/; Max-Age=0;',
                    'key=; Path=/; Max-Age=0;',
                    'loginFlag=; Path=/; Max-Age=0;'
                ]}, {type:'html'});
            },
            err =>{
                console.error(err);
                response(res, statusCode.InternalServerError, '500エラーだよ',{}, {type:'html'});
            });
        });
    });
});
function requireLogin(res, callback){
    let req = res.req,
        cookie = parseCookie(req.headers.cookie),
        name = cookie&&cookie.name,
        loginFlag = cookie&&cookie.loginFlag,
        key = cookie&&cookie.key,
        redirectURL = req.url.match(/^\/login[1-9]/) ? redirectURL : '/login4';
    if(req.url == '/requireLogin1'){
        if(!name||!loginFlag){
            response(res, statusCode.SeeOther, '', {
                Location:'/login1',
                'Set-Cookie':[
                    `url=${escape(req.url)}; Path=/login1`,
                    'uid=; Max-Age=0; Path=/;',
                    'key=; Max-Age=0; Path=/;'
                ]
            });
        }else if(loginFlag === '1'){
            callback(name, undefined, key);
        }else{
            response(res, statusCode.SeeOther, '', {Location:`/login1`, 'Set-Cookie':[`url=${escape(req.url)}; Path=/login1`, 'uid=; Max-Age=0; Path=/;', 'key=; Max-Age=0; Path=/;']});
        }
    }
    else if(!name||!key){
        response(res, statusCode.SeeOther, '', {Location:redirectURL, 'Set-Cookie':[`url=${escape(req.url)}; Path=${redirectURL}`, 'uid=; Max-Age=0; Path=/;', 'key=; Max-Age=0; Path=/;']});
    }else{
        checkSession(name, key, req.url).then(uid=>callback(name, uid, key), err => {
            if(!err){
                response(res, statusCode.SeeOther, '', {Location:redirectURL, 'Set-Cookie':[`url=${escape(req.url)}; Path=${redirectURL}`, 'uid=; Max-Age=0; Path=/;', 'key=; Max-Age=0; Path=/;']});
            }else{
                response(res, statusCode.InternalServerError, String(err));
                throw err;
            }
        });
    }
}
function checkSession(name, key, url){
    return new Promise((resolve, reject)=>{
        if(url === '/requireLogin2'){
            client.query(`SELECT uid from login WHERE uid=(SELECT uid FROM users WHERE name=\'${name}\') AND key=\'${key}\'`).then(rslt =>{
                if(rslt.rows.length === 0){
                    reject();
                }else{
                    client.query('UPDATE login SET t=now() WHERE uid=$1 AND key=$2',[rslt.rows[0].uid, key]).then(rslt2 => {
                        resolve(rslt.rows[0].uid);
                    }, err => {
                        reject(err);
                        throw err;
                    });
                }
            },err => {
                reject(err);
                throw err;
            });
        }else{
            client.query('SELECT uid from login WHERE uid=(SELECT uid FROM users WHERE name=$1) AND key=$2',[name, key]).then(rslt =>{
                if(rslt.rows.length==1){
                    client.query('UPDATE login SET t=now() WHERE uid=$1 AND key=$2',[rslt.rows[0].uid, key]).then(rslt2 => {
                        resolve(rslt.rows[0].uid);
                    }, err => {
                        reject(err);
                        throw err;
                    });
                }else{
                    reject();
                }
            },err => {
                reject(err);
                throw err;
            });
        }
    });
}
function login(name, pw, url){
return new Promise((resolve, reject)=>{
if(url === '/login1'){
    client.query(`SELECT uid from users WHERE name=\'${name}\' AND pw=\'${pw}\'`).then(rslt =>{
        if(rslt.rows.length === 0){
            reject('Invalid UserName or PW');
            return;
        }
        resolve(undefined);
    });
}else if(url === '/login2'){
    client.query(`SELECT uid from users WHERE name=\'${name}\' AND pw=\'${pw}\'`).then(rslt =>{
        if(rslt.rows.length === 0){
            reject('Invalid UserName or PW');
            return;
        }
        let uid = rslt.rows[0].uid;
        (function insertLogin(){
            crypto.randomBytes(12, (err,bytes)=>{
                let key = bytes.toString('hex');
                client.query('INSERT INTO login(key, uid) values($1, $2)', [key, uid])
                .then(rslt2 => {
                    client.query('SELECT FROM login WHERE uid=$1', [uid])
                    .then(rslt =>{
                        if(rslt.rows.length > 3){
                            client.query('DELETE FROM login WHERE uid=$1 AND key=(SELECT key FROM login WHERE uid=$1 ORDER BY t ASC LIMIT 1);',[uid])
                            .then(rslt =>{
                                resolve(key);
                            },err => {
                                console.error(err.toString());
                                reject(err);
                            })
                        }else{
                            resolve(key);
                        }
                    },err =>{
                        console.error(err.toString());
                        reject(err);
                    });
                },err => {
                    console.error(err.toString());
                    reject(err);
                });
            });
        })();
    }, err => {
        console.error(err.toString());
        reject(err);
    });
}else if(url === '/login3'){
    client.query('SELECT uid from users WHERE name=$1 AND pw=$2', [name, pw]).then(rslt =>{ //静的プレースホルダー
        if(rslt.rows.length === 0){
            reject('Invalid UserName or PW');
            return;
        }
        let uid = rslt.rows[0].uid;
        (function insertLogin(){
            crypto.randomBytes(12, (err,bytes)=>{
                let key = bytes.toString('hex');
                client.query('INSERT INTO login(key, uid) values($1, $2)', [key, uid])
                .then(rslt2 => {
                    client.query('SELECT FROM login WHERE uid=$1', [uid])
                    .then(rslt3 =>{
                        if(rslt3.rows.length > 3){
                            client.query('DELETE FROM login WHERE uid=$1 AND key=(SELECT key FROM login WHERE uid=$1 ORDER BY t ASC LIMIT 1);',[uid])
                            .then(rslt4 =>{
                                resolve(key);
                            },err => {
                                console.error(err.toString());
                                reject(err);
                            })
                        }else{
                            resolve(key);
                        }
                    },err =>{
                        console.error(err.toString());
                        reject(err);
                    });
                },err => {
                    console.error(err.toString());
                    reject(err);
                });
            });
        })();
    }, err => {
        console.error(err.toString());
        reject(err);
    });
} else if(url == '/login4' || url === '/registerUser'){
    client.query('SELECT uid, hash from users WHERE name=$1', [name]).then(rslt =>{
        if(rslt.rows.length === 0){
            reject('Invalid UserName or PW');
            return;
        }
        let user = rslt.rows[0];
        bcrypt.compare(pw, user.hash, (err, ok) => {
            if(err){
                console.error(err.toString());
                reject(err);
                return;
            }
            if(ok){
                (function insertLogin(){
                    crypto.randomBytes(12, (err,bytes)=>{
                        let key = bytes.toString('hex');
                        client.query('INSERT INTO login(key, uid) values($1, $2)', [key, user.uid])
                        .then(rslt2 => {
                            client.query('SELECT FROM login WHERE uid=$1', [user.uid])
                            .then(rslt3 =>{
                                if(rslt3.rows.length > 3){
                                    client.query('DELETE FROM login WHERE uid=$1 AND key=(SELECT key FROM login WHERE uid=$1 ORDER BY t ASC LIMIT 1);',[user.uid])
                                    .then(rslt4 =>{
                                        resolve(key);
                                    },err => {
                                        console.error(err.toString());
                                        reject(err);
                                    })
                                }else{
                                    resolve(key);
                                }
                            },err =>{
                                console.error(err.toString());
                                reject(err);
                            });
                        },err => {
                            console.error(err.toString());
                            reject(err);
                        });
                    });
                })();
            }else{
                reject('Invalid UserName or PW');
            }
        });
    }, err => {
        console.error(err.toString());
        reject(err);
    });
}else{
    reject(new Error(`Invalid URL : ${req.url}`));
}
});
}

function registerUser(name, pw){
return new Promise((resolve, reject)=>{
    bcrypt.hash(pw, bcryptRounds)
        .then(hash => {
            client.query('INSERT INTO USERS(name, pw, hash) VALUES($1, $2, $3)',[name, pw, hash]).then(resolve,reject);
        },err => {
            reject('Unknown Error');
            throw err;
        }
    );
});
}
String.prototype.bytes = function(){
    return Buffer.from(this).length;
}
function response(res, code, data, head_arg={}, options={}){
try{
    if(!Buffer.isBuffer(data)){
        if(data === void(0)){
            data = 'undefined';
        }else if(data === null){
            data = 'null';
        }else if(data.toString){
            data = data.toString();
        }else{
            data = String(data);
        }
    }
    let header = {
        'Server'        : 'Node.js',
        'Date'          : new Date().toGMTString(),
        'Content-Type'  : 'text/plain',
        'Content-Length': data.bytes().toString(),
        ...head_arg
    };
    switch(options.type){
        case undefined:
            break;
        case 'html':
            header['Content-Type'] = 'text/html; charset=UTF-8';
            break;
        case 'css':
            header['Content-Type'] = 'text/css; charset=UTF-8';
            break;
        case 'js':
            header['Content-Type'] = 'text/javascript; charset=UTF-8';
            break;
        case 'json':
            header['Content-Type'] = 'application/json; charset=UTF-8';
            break;
        default:
            throw new Error(`Unknown Type: ${type}`);
    }
    if(!header['Transfer-Encoding']&&!header['transfer-encoding']){
        res.removeHeader('Transfer-Encoding');
    }
    res.writeHead(code, header);
    res.write(data);
    res.end();
}catch(err){
    res.end();
    throw err;
}
}
function parseCookie(cookie){
    if(!cookie) return {};
    let rtn = {};
    cookie.split(/;/g).forEach(e => {
        if(e=='')return;
        rtn[e.split('=')[0].replace(/ /g,'')] = unescape(e.split('=')[1]);
    });
    return rtn;
}
server.listen(PORT);
console.log(`Listening on port ${PORT} without SSL...`);
}
