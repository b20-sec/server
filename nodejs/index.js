const fs = require('fs'),
    http = require('http'),
    https = require('https'),
    express = require('express'),
    app = express();

app.use(express.static('../public'));
// app.listen(80);
app.listen(3000);
console.log('listening on Port 3000...');