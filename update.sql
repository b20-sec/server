DROP TABLE users;
CREATE TABLE users(
    uid serial primary key,
    name varchar(255) not null unique,
    pw varchar(255) not null,
    hash varchar(60) not null
);
DROP TABLE login;
CREATE TABLE login(
    uid int,
    key varchar(24),
    t timestamp default CURRENT_TIMESTAMP not null, 
    primary key(uid, key)
);
DROP TABLE product;
CREATE TABLE product(
    pid serial primary key,
    value int not null,
    name varchar(59) not null unique
);

DROP TABLE bbs;
CREATE TABLE bbs(
    id serial primary key,
    t timestamp default CURRENT_TIMESTAMP not null, 
    author varchar(50) not null,
    text varchar(140) not null
);

INSERT INTO product(value, name) VALUES(100,'ゆでたまご');
INSERT INTO product(value, name) VALUES(200,'アイス');
INSERT INTO product(value, name) VALUES(300,'とうもろこし');
INSERT INTO product(value, name) VALUES(400,'納豆');
INSERT INTO product(value, name) VALUES(500,'豚汁');
INSERT INTO product(value, name) VALUES(600,'ビックマック');
INSERT INTO product(value, name) VALUES(931,'三種のチーズ牛丼');
INSERT INTO product(value, name) VALUES(50,'豚肉');
INSERT INTO product(value, name) VALUES(934500,'牛肉');
INSERT INTO product(value, name) VALUES (34500,'ポテトLサイズ');
